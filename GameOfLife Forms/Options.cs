﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GameOfLife_Forms
{
    public partial class Options : Form
    {
        Form1 form1;
        public Options(Form1 form1)
        {
            InitializeComponent();
            this.form1 = form1;
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            
            int rows = int.Parse(textBoxRows.Text);
            int columns = int.Parse(textBoxColumns.Text);
            int percentage = int.Parse(textBoxPercentage.Text);
            int interval = int.Parse(textBoxInterval.Text);

            if (rows > 0)
            {
                form1.Rows = rows;
            }
            if (columns > 0)
            {
                form1.Columns = columns;
            }
            if (percentage > 0 && percentage < 101)
            {
                form1.Percentage = percentage;
            }
            if (interval > 0)
            {
                form1.timerInterval = interval;
            }
            this.Close();


        }

        private void Options_FormClosed(object sender, FormClosedEventArgs e)
        {

        }
    }
}
