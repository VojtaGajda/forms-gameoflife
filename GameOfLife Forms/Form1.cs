﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using System.Reflection;

namespace GameOfLife_Forms
{
    public partial class Form1 : Form
    {
        

        public int Rows { get; set; } = 30;
        public int Columns { get; set; } = 30;
        public double Percentage { get; set; } = 30;
        public int timerInterval { get; set; } = 200;

        int cellHeight;
        int cellWidth;
        bool isPlaying;
        Timer mainTimer;
        int stepsCount;

        public Form1()
        {
            InitializeComponent();
            Init();
        }

        private void Init()
        {
            isPlaying = false;
            mainTimer = new Timer();
            mainTimer.Interval = timerInterval;
            mainTimer.Tick += new EventHandler(UpdateStates);
            labelSteps.Text = stepsCount.ToString();
            labelGameEnd.Visible = false;
            //currentState = InitMap();
            //nextState = InitMap();
            //InitCells();
        }
         
        private void UpdateStates(object sender, EventArgs e)
        {
            if (isPlaying)
            {
                NextState();
                UpdateGame();
                labelSteps.Text = stepsCount.ToString();
            }
            else
            {
                mainTimer.Stop();
                //MessageBox.Show($"Game ended. (Number of steps it took: {stepsCount})");
                labelGameEnd.Visible = true;
                if (stepsCount == 1)
                {
                    labelGameEnd.Text = $"GAME ENDED (it took {stepsCount} step.)";
                }
                else
                {
                    labelGameEnd.Text = $"GAME ENDED (it took {stepsCount} steps.)";
                }
                ClearGrid();
                stepsCount = 0;
                labelSteps.Text = stepsCount.ToString();
            }

        }

        void SetFormSize()
        {
            this.Width = panelGame.Width + 38;
            this.Height = panelGame.Height + panel1.Height + settingsToolStripMenuItem.Height + 55;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            ReloadData();
        }

        private void ReloadData()
        {
            cellHeight = 900 / Rows;
            cellWidth = 900 / Columns;
            panelGame.Controls.Clear();
            GenerateGrid(Rows, Columns, Percentage);
            SetFormSize();
        }

        void GenerateLiveCells(int Rows, int Columns, double Percentage)
        {
            int totalElements = Rows * Columns;
            int numElementsToChange = (int)Math.Round(totalElements * Percentage / 100.0);
            //int numElementsToChange = 5;
            Random random = new Random();
            List<liveCell> listLiveCell = new List<liveCell>();

            for (int i = 0; i < numElementsToChange; i++)
            {
             select:
                liveCell c = new liveCell(random.Next(0, Columns), random.Next(0, Rows));
                if (listLiveCell.SingleOrDefault(a => a.x == c.x && a.y == c.y) != null)
                    goto select;
                else
                    listLiveCell.Add(c);

            }

            foreach(Cell cell in panelGame.Controls)
            {
                if (listLiveCell.SingleOrDefault(a => a.x == cell.x && a.y == cell.y) != null)
                {
                    cell.isAlive = true;
                    cell.BackColor = Color.Green;
                }
            }

            UpdateGame();
        }

        void GenerateGrid(int Rows, int Columns, double Percentage)
        {
            panelGame.Height = Rows * cellHeight;
            panelGame.Width = Columns * cellWidth;

            for (int c = 0; c < Columns; c++)
            {
                for (int r = 0; r < Rows; r++)
                {
                    Cell cell = new Cell
                    {
                        Width = cellWidth,
                        Height = cellHeight,
                        x = c,
                        y = r,
                        Margin = new Padding(0),
                        //Padding = new Padding(0),
                        Font = new Font("Cairo", 12, FontStyle.Bold, GraphicsUnit.Point),
                        BackColor = Color.Gray,
                        //Location = new Point(c * 30, r * 30),
                        Location = new Point(c * cellWidth, r * cellHeight),
                        FlatStyle = FlatStyle.Flat
                    };
                    panelGame.Controls.Add(cell);
                    cell.MouseDown += Cell_MouseDown;
                }
            }
            UpdateGame();
            
        }
        
        void ClearGrid()
        {
            foreach (Cell cell in panelGame.Controls)
            {
                cell.isAlive = false;
            }
            UpdateGame();
        }

        void UpdateGame()
        {
            foreach (Cell cell in panelGame.Controls)
            {
                //cell.Text = cell.liveNeighbours.ToString();
                if (cell.isAlive)
                { 
                    cell.BackColor = Color.LawnGreen; 
                }
                else
                { 
                    cell.BackColor = Color.DimGray;
                    
                }
            }
        }

        void NextState()
        {
            int changes = 0;
            List<Cell> gameBoard = new List<Cell>();
            foreach (Cell item in panelGame.Controls)
            {
                gameBoard.Add(item);
            }
            bool[,] newBoard = new bool[Columns, Rows];
            foreach (Cell cell in gameBoard)
            {
                if (cell.isAlive)
                {
                    newBoard[cell.x, cell.y] = true;
                }
            }

            foreach (Cell cell in gameBoard)
            {
                if (cell.isAlive)
                {
                    //Each cell with one or no neighbors dies, as if by solitude.
                    //OR
                    //Each cell with four or more neighbors dies, as if by overpopulation.
                    if (cell.liveNeighbours <= 1 || cell.liveNeighbours >= 4)
                    {
                        newBoard[cell.x, cell.y] = false;
                        changes++;
                    }
                    //Each cell with two or three neighbors survives.
                }
                else
                {
                    if (cell.liveNeighbours == 3)
                    {
                        newBoard[cell.x, cell.y] = true;
                        changes++;
                    }
                }

            }
            foreach (Cell cell in panelGame.Controls)
            {
                cell.isAlive = newBoard[cell.x, cell.y];
            }
            if(changes == 0)
            {
                isPlaying = false;
            }
            else
            {
                stepsCount++;
            }
        }

        private void Cell_MouseDown(object sender, MouseEventArgs e)
        {
            Cell cell = sender as Cell;
            if (cell != null)
            {
                if (cell.isAlive)
                {
                    cell.BackColor = Color.Gray;
                    cell.isAlive = false;
                }
                else
                {
                    cell.BackColor = Color.Green;
                    cell.isAlive = true;
                }

            }
            UpdateGame();
            stepsCount = 0;
            labelSteps.Text = stepsCount.ToString();
        }

        private void generateGridToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Options options = new Options(this);
            options.FormClosed += new FormClosedEventHandler(Options_FormClosed);
            options.ShowDialog();

        }

        private void Options_FormClosed(object sender, FormClosedEventArgs e)
        {
            ReloadData();
        }

        private void startToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (!isPlaying)
            {
                isPlaying = true;
                mainTimer.Start();
                labelGameEnd.Visible = false;
            }
            //NextState();
        }

        private void newGridToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ClearGrid();
            GenerateLiveCells(Rows, Columns, Percentage);
            stepsCount = 0;
            labelSteps.Text = stepsCount.ToString();
            labelGameEnd.Visible = false;
        }

        private void clearGridToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Stop();
            ClearGrid();
            stepsCount = 0;
            labelSteps.Text = stepsCount.ToString();
            labelGameEnd.Visible = false;
        }

        private void stopToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Stop();
        }

        private void Stop()
        {
            mainTimer.Stop();
            isPlaying = false;
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Stop();
            bool[,] data = new bool[Columns, Rows];
            foreach(Cell cell in panelGame.Controls)
            {
                if (cell.isAlive)
                {
                    data[cell.x, cell.y] = true;
                }
                else
                {
                    data[cell.x, cell.y] = false;
                }

            }
            string json = JsonConvert.SerializeObject(data);

            string resourcesPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            string saveFilePath = Path.Combine(resourcesPath, "save.json");
            File.WriteAllText(saveFilePath, json);
        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Stop();
            stepsCount = 0;
            labelSteps.Text = stepsCount.ToString();
            labelGameEnd.Visible = false;

            string resourcesPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            string loadFilePath = Path.Combine(resourcesPath, "save.json");
            string json = File.ReadAllText(loadFilePath);
            bool[,] loadedBoard = JsonConvert.DeserializeObject<bool[,]>(json);
            Columns = loadedBoard.GetLength(0);
            Rows = loadedBoard.GetLength(1);
            ReloadData();
            foreach (Cell cell in panelGame.Controls)
            {
                cell.isAlive = loadedBoard[cell.x, cell.y];
            }
            UpdateGame();
        }
    }
}
