﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameOfLife_Forms
{
    public class Cell : Button
    {
        public int x { get; set; }
        public int y { get; set; }
        public bool isAlive { get; set; }
        public int liveNeighbours
        {
            get
            {
                int count = 0;
                List<Cell> cells = new List<Cell>();
                foreach (Cell cell in Parent.Controls)
                {
                    cells.Add(cell as Cell);
                }
                foreach (Cell neighborCell in cells)
                {
                    if (neighborCell != this && Math.Abs(neighborCell.x - x) <= 1 && Math.Abs(neighborCell.y - y) <= 1 && neighborCell.isAlive)
                    {
                        count++;
                    }
                }
                return count;


                //int count = cells.Where(a => (a.x == x || a.x == x + 1 || a.x == x - 1) && (a.y == y || a.y == y + 1 || a.y == y - 1)).Where(a => a.isAlive).Count();
                //int count = cells.Where(a => (a.x != x) && (a.y !=y)).Where(a => (a.x == x || a.x == x + 1 || a.x == x - 1) && (a.y == y || a.y == y + 1 || a.y == y - 1)).Where(a => a.isAlive).Count();
                return count;
            }
        }

    }

    public class liveCell
    {
        public int x { get; set; }
        public int y { get; set; }
        public liveCell(int x, int y)
        {
            this.x = x;
            this.y = y;
        }
    }
}
